# Projet Java 

* ** Moussa Ndiaye** - *Eleve ingenieur en Telecommunications et reseaux*



# Serie 1

## Exercice 0
>Question 1:

Pour afficher le nom du thread principal, on utilise la commande ``Thread.currentThread().getName()``

>Question 3:

Le nom du thread est ``Thread-0``
>Question 4:

Le nom du thread est ``pool-1-thread-1``



## Exercice 1
>Question 2:

Lorsqu'on crée une instance unique de Thread qui execute la tache de compter jusqu'a 100, on arrive bien à compter jusqu'a 100

>Question 3:

A l'execution on obtient valeur variable. Cela s'explique par le fait que les 10 threads cherchent à incrementer 100 la même valeur donc il y a un acces conccurent au niveau du champs compteur. Cela engendre des echecs lors de l'incrementation ce qui explique le fait qu'on atteint jamais la bonne valeur. 
>Question 4:

Pour resoudre ce probleme on peut poser un verrou au niveau de l'incrementation afin d'eviter les acces conccurent.
>Question 5:

L'utilisation de la clases AtomicLong est beaucoup plus dacime pour la gestion des acces conccurent car les classes atomiques n'utilisent pas de synchronisation; elles sont ainsi très performantes.
Comme on utilise pas la synchonisation, on pourra aussi eviter de tomber dans des death locks 


## Exercice 2
>Question 5:

Comme strategie j'ai prevu de bloquer l'operation de ``add()`` (respectivement ``remove()``) dans le cas ou l'entrepot est plein (respectivement vide). Cet attente ses fera à l'aide de la fonction wait qu'on posera sur le ``lock`` de la synchronisation. Une fois la ressource disponible on pourra effectuer l'operation de add/remove et faire un ``notifyAll()`` pour permettre à l'un des processus en attente de pouvoir s'executer.

>Question 7:

Dans cette partie on a crée un unique pool de thread de taille 5. Lorsqu'on lui soumet les  100 taches de ``add()`` et **95** taches de ``remove()``,le resultat de l'execution est 10 et le programme reste bloqué.

En effet, comme on avait fixé la capacité de l'entrepot à 10, les 10 premiers operations de ``add()`` vont bien se derouler. Ensuite la 11e jusqu'a la 15e tache, qui sont eux aussi des taches ``add()``, vont faire un ``lock.wait()``, bloquant les 5 threads. 

Le programme reste bloqué dans cet etat de ``death locks``.
>Question 8:

Pour garantir que toutes les operations seront traitées, j'ai decidé d'utiliser non pas un mais deux pools de threads la premaire, de taille 3, va permettre d'effectuer les taches ``add()`` la seconde ,de taille 2, va permettre de faire les taches ``remove()``

On a un resultat correct.

# Serie 2
>Question 1:

Si un deuxieme client se connecte il ne sera pas pris en compte par le serveur car pour le moment le serveur ne sait repondre qu'a un client.

On doit rendre le serveur multitache:  cad le rendre capable de communiquer avec plusieurs client à la fois

>Question 2:

Afin de rendre notre serveur multithread, on doit creer la tache à executer pour chaque thread à partir de la ligne 15.
```           
 
15            Socket socket = server.accept();

```

>Question 3:

Afin de bien gerer les threads clients et eviter d'en creer une quantité enorme, on utilise un pool de thread de taille **n** et on realloue aux nouveaux clients les threads disponibles


>Question 5:

Lorsqu'un grand nombre de clients manipulent la collections ils se pourrait les deux tentent d'acceder à la meme valeur pour la modifier. Ceci est tres embetant car il pourrait conduire à un ``ConcurrentModificationException``. 
Pour eviter ce genre d'incident, la ``ConccurentHashMap`` gere les acces conccurents au niveau de notre Map