package Serie2;

import Serie1.Exercice2.Product;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {

    public static void main(String[] args) throws IOException {

        ServerSocket server = new ServerSocket(8080);
        ExecutorService clientServices = Executors.newFixedThreadPool(2);
        int i = 0;
        Map<String, Integer> annonces = new ConcurrentHashMap<>();
        Map<Long, Product> productMap = new ConcurrentHashMap<>();

        while (true) {
            int numeroClient = i++;

            System.out.println("Listening to request");
            Socket socket = server.accept();
            System.out.println("Accepting request " + numeroClient);
            Callable tClient = () -> {
                InputStream inputStream = socket.getInputStream();
                BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

                OutputStream outputStream = socket.getOutputStream();
                PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream));

                String order = reader.readLine();
                while (order != null) {
                    if (order.toUpperCase().startsWith("GET")) {
                        writer.printf("1\n");
                        writer.printf("Received GET order : %s\n", order);
                        writer.flush();
                        System.out.printf("Received GET order : %s from client %d\n", order, numeroClient);
                    } else if (order.toUpperCase().startsWith("PUT")) {
                        String key = getKey(order);
                        Integer value = getInteger(order, 2);
                        annonces.put(key, value);
                        writer.printf("1\n");
                        writer.printf("Annonce ajoute  %s %s\n", key, value);
                        writer.flush();
                        System.out.printf("Received PUT order : %s from client %d\n", order, numeroClient);
                    } else if (order.toUpperCase().startsWith("LIST")) {
                        //Premiere version de la commande LIST
                        /*
                        writer.printf("%d\n", annonces.size());
                        System.out.println("Received LIST order :");
                        for (Map.Entry<String, Integer> entry : annonces.entrySet()) {
                            writer.printf("%s %s\n", entry.getKey(), entry.getValue());
                            System.out.printf("%s %s\n", entry.getKey(), entry.getValue());
                        }
                         */
                        //Deuxieme version de la commande LIST
                        writer.printf("%d\n", productMap.size());

                        System.out.println("Received LIST order :");
                        for (Map.Entry<Long, Product> entry : productMap.entrySet()) {
                            Product p = entry.getValue();
                            writer.printf("%d %s %d\n", entry.getKey(), entry.getValue().getName(), entry.getValue().getPrice());
                            System.out.printf("%d %s %d\n", entry.getKey(), entry.getValue().getName(), entry.getValue().getPrice());
                        }
                        writer.flush();
                    }
                    else if (order.toUpperCase().startsWith("BUY")) {

                        writer.printf("1\n");
                        // Version 1 commande BUY
                        /*
                        String key = getKey(order);
                        System.out.printf("Received BUY order : %s\n", key);
                        if (annonces.remove(key) != null) {
                            writer.printf("Achat conffirmer, Votre article %s est en cours de livraison\n", key);
                            System.out.println("Achat confirmé pour le client " + numeroClient);
                        } else {
                            writer.printf("Desolé, article %s deja vendu ou inexistant\n", key);
                            System.out.println("Achat non confirmé pour le client " + numeroClient);
                        }

                         */
                        // Version 2 commande BUY
                        Product p;
                        long key = getLong(order, 1);
                        System.out.printf("Received BUY order : %s\n", key);
                        if ((p = productMap.remove(key)) !=null){
                            writer.printf("Achat conffirmer id : %d  nom: %s prix: %d\n", p.getId(),p.getName(),p.getPrice());
                            System.out.printf("Achat conffirmer id : %d  nom: %s prix: %d pour le client %d\n", p.getId(),p.getName(),p.getPrice(),numeroClient);
                        } else{
                            writer.printf("Desolé, article %s deja vendu ou inexistant\n", key);
                            System.out.println("Achat non confirmé pour le client \n" + numeroClient);
                        }
                        writer.flush();
                    } else if (order.toUpperCase().startsWith("CREATE")) {
                        String nomObjet = order.substring(order.indexOf(" ") + 1);
                        Product p;
                        synchronized (lock) {
                            codeProduit++;
                            p = new Product(nomObjet, codeProduit);
                        }
                        System.out.println("CREATED " + p.getName() + " id:" + p.getId());
                        writer.printf("1\n");
                        writer.printf("CREATED %d\n", p.getId());
                        writer.flush();
                        productMap.put(p.getId(), p);
                    } else if (order.toUpperCase().startsWith("PRICE")) {
                        long id = getLong(order, 1);
                        int prix = getInteger(order, 2);
                        writer.printf("1\n");

                        if (productMap.containsKey(id)) {
                            Product p = productMap.get(id);
                            p.setPrice(prix);
                            writer.printf("Objet ( %d ) %s prix fixé à %d $\n", p.getId(), p.getName(), p.getPrice());
                            System.out.println("Objet ( " + p.getId() + " ) " + p.getName() + " prix fixé à " + p.getPrice() + " by " + numeroClient);
                        } else {
                            writer.printf("Objet %d inexistant \n", id);
                            System.out.println("Objet " + id + " inexistant");
                        }
                        writer.flush();
                    } else if (order.toLowerCase().equals("bye") || order.toLowerCase().equals("quit")) {
                        System.out.println("Closing connection" + numeroClient);
                        socket.close();
                    }
                    order = reader.readLine();
                }
                return null;
            };

            clientServices.submit(tClient);
        }
    }

    private static String getKey(String order) {
        return order.split("\\s")[1];
    }

    private static Integer getInteger(String order, int i) {
        return Integer.valueOf(order.split("\\s")[i]);
    }

    private static Long getLong(String order, int i) {
        return Long.valueOf(order.split("\\s")[i]);
    }

    private static long codeProduit = 0;
    private final static Object lock = new Object();
}