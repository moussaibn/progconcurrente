package Serie2;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    public static void main(String[] args) throws IOException {

        Socket socket = new Socket("localhost", 8080);

        InputStream inputStream = socket.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

        OutputStream outputStream = socket.getOutputStream();
        PrintWriter writer = new PrintWriter(new OutputStreamWriter(outputStream));

        System.out.print("> ");
        Scanner scanner = new Scanner(System.in);
        String command = scanner.nextLine();

        while (!"bye".equals(command)) {

            System.out.println("Sending command = " + command);
            writer.println(command);
            writer.flush();

            int nbline=Integer.parseInt(reader.readLine());
            while(nbline>0){
                System.out.println(reader.readLine());
                nbline--;
            }
            System.out.print("> ");
            command = scanner.nextLine();
        }
        writer.println(command);
        writer.flush();
    }
}