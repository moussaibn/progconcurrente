package Serie1.Exercice0;


import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Exercice0 {
    public static void main(String[] args) {
        //Question 1
        System.out.println("Le nom du Thread principal est " + Thread.currentThread().getName());

        //Question 2
        Runnable t = () -> System.out.println("Le nom du Thread m'execute est " + Thread.currentThread().getName());

        //Question 3
        Thread thread = new Thread(t);
        thread.start();

        //Question 4
        ExecutorService executorService = Executors.newSingleThreadExecutor();
        executorService.execute(t);
        executorService.shutdown();
    }
}
