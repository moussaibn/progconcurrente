package Serie1.Exercice1;

public class Exercice1 {
    public static void main(String[] args) throws InterruptedException {
        //Compteur compteur = new Compteur();
        //Object lock = new Object();
        AtomicCompteur compteur= new AtomicCompteur();
        Runnable tache = () ->
        {
            for (int i = 0; i < 1000; i++) {
          //      synchronized (lock) {
                    compteur.incrementer();
            //    }
            }
        };

        System.out.println("Compteur : "+ compteur.getCompteur());

        Thread [] threads = new Thread[10];
        for (int i = 0; i < 10; i++) {
            threads[i]= new Thread(tache);
        }
        for (int i = 0; i < 10; i++) {
            threads[i].start();
        }

        for (int i = 0; i < 10; i++) {
            threads[i].join();
        }
        System.out.println("Compteur : "+ compteur.getCompteur());

    }
}
