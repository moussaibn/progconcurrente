package Serie1.Exercice1;

public class Compteur {
    private int compteur=0;

    public void incrementer(){
        compteur++;
    }

    public int getCompteur() {
        return compteur;
    }
}
