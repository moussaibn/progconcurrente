package Serie1.Exercice2;

public class Warehouse {
    private final Object lock = new Object();
    private int capacity;

    private int nbCaisse;

    public void incrementer() {
        nbCaisse++;
    }

    public void decrementer() {
        nbCaisse--;
    }


    public Warehouse(int capacity) {
        this.capacity = capacity;
        nbCaisse = 0;
    }

    public void add() {
        synchronized (lock) {
            while (this.content() == capacity) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            incrementer();
            lock.notifyAll();
        }
    }

    public void remove() {
        synchronized (lock) {
            while (this.content() == 0) {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            decrementer();
            lock.notifyAll();
        }
    }

    public int content() {
        return nbCaisse;
    }

}
