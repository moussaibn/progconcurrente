package Serie1.Exercice2;

import java.util.Objects;

public class Product {
    private String name;
    private int price;
    private long id;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public Product(String name, long id) {
        this.name = name;
        this.id = id;
    }

    @Override
    public String toString() {
        return "id : " + getId() + " name : " + getName() + " price:" + getPrice();
    }
}
