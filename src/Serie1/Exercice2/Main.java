package Serie1.Exercice2;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        int capacity = 10;
        Warehouse warehouse = new Warehouse(capacity);
        Runnable addInstance = warehouse::add;
        Runnable removeInstance = warehouse::remove;

        /*Thread t1=new Thread(addInstance);
        Thread t2= new Thread(removeInstance);
        t1.start();
        t1.join();
        System.out.println("warehouse = " + warehouse.content());
        t2.start();
        t2.join();
        */

        System.out.println("warehouse = " + warehouse.content());
        //Question 6:
        //AddTasks

        ExecutorService executorServiceForAdds = Executors.newFixedThreadPool(3);
        ExecutorService executorServiceForRemoves = Executors.newFixedThreadPool(2);
        for (int i = 0; i < 100; i++) {
            executorServiceForAdds.submit(addInstance);
        }
        for (int i = 0; i < 95; i++) {
            executorServiceForRemoves.submit(removeInstance);
        }
        executorServiceForAdds.shutdown();
        executorServiceForRemoves.shutdown();
        Thread.sleep(500);

        System.out.println("warehouse = " + warehouse.content());

    }
}
